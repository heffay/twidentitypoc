﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata.Ecma335;
using System.Threading.Tasks;
using IdentityModel;
using IdentityServer4.Validation;

namespace CoreIdentityServer
{
    public class VeteranExtensionGrantValidator : IExtensionGrantValidator
    {
        private readonly IVeteranValidatorService _veteranValidator;

        public string GrantType => "VeteranExtensionGrantValidator";

        public VeteranExtensionGrantValidator(IVeteranValidatorService veteranValidator)
        {
            _veteranValidator = veteranValidator;
        }

        public async Task ValidateAsync(ExtensionGrantValidationContext context)
        {
            var phone = context.Request.Raw.Get("phone");
            var zip = context.Request.Raw.Get("zip");
            var name = context.Request.Raw.Get("name");

            var result = await _veteranValidator.IsValid(phone, zip, name);
            if (result)
            {
                context.Result = new GrantValidationResult("some_user_id", GrantType);
            }

            context.Result = new GrantValidationResult(OidcConstants.TokenErrors.InvalidClient, "not_authorized");
        }

    }
}
