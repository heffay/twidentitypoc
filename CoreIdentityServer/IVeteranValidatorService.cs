﻿using System.Threading.Tasks;

namespace CoreIdentityServer
{
    public interface IVeteranValidatorService
    {
        Task<bool> IsValid(string phone, string zip, string name);
    }

    class VeteranValidatorService : IVeteranValidatorService
    {
        public Task<bool> IsValid(string phone, string zip, string name)
        {
            //TOOD: do the web service call here
            return Task.FromResult(phone.Equals("4807975589") && zip.Equals("85085") && name.Equals("Geoff"));
        }
    }
}